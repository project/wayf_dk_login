<?php
/**
 * @file
 * Template for the login content pane
 */
?>
<div class="wayf-dk-login">
  <?php print render($login_url) ?>
</div>
